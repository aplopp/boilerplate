<?php
$notFoundPage = get_theme_options( 'theme_options', 'not_found_page' );
if ( $notFoundPage ){
	query_posts( array(
		'post_type' => 'page',
		'page_id' => $notFoundPage
	));
	global $post;
	$post = get_post( $notFoundPage );
	include 'page.php';
	die;
} else {
	echo 'Page not found.';
	die;
}