<?php global $Forms;

if ( ! $Forms ) return;

$Forms->add_page( 'theme_options', array(
	'menu_title' => 'Theme Options',
	'first_subpage_menu_title' => 'General',
	'title' => 'General',
	'fields' => array(
		'not_found_page' => array(
			'type' => 'post',
			'post_type' => 'page'
		),
		'favicon' => array(
			'type' => 'media',
			'size' => 'medium',
			'get' => 'url'
		)
	),
	'subpages' => array(
		'header' => array(
			'fields' => array(
				'logo' => array(
					'type' => 'media',
					'size' => 'large',
					'get' => 'image'
				)
			)
		),
		'sidebar' => array(
			'fields' => array(
				'sidebars' => array(
					'description' => 'WARNING: changing the sidebar name will remove the existing widgets from the sidebar',
					'cloneable' => true,
					'required' => true
				)
			)
		),
		'footer' => array(
			'fields' => array(
				'copyright' => array(
					'description' => '[year] will be replaced by current year',
					'default' => '&copy; [year] '.get_bloginfo( 'name' ),
					'size' => 100
				)
			)
		)
	)
));

// exclude "not found" page from search results
add_filter('pre_get_posts', function($query) {
    if ($query->is_search) {
    	$not_found_page = get_theme_options( 'theme_options', 'not_found_page' );
    	if ( $not_found_page ){
        	$query->set('post__not_in', array( $not_found_page ) );
    	}
    }
    return $query;
});
