<?php
function theme_get_colors(){
	return array(
		'blue' => array(
			'label' => 'Blue',
			'color' => '#006CA1'
		),
		'orange' => array(
			'label' => 'Orange',
			'color' => '#f68c3d'
		),
		'gray' => array(
			'label' => 'Gray',
			'color' => '#666'
		)
	);
}
// meant to be used with a radio input
function theme_get_colored_options(){
	$color_options;
	foreach( theme_get_colors() as $val => $color ){
		$color_options[ $val ] = '<span style="display: inline-block; width: 1em; height: 10px; border-radius: 3px; margin-right: 5px; background: '.$color['color'].';"></span>';
		$color_options[ $val ] .= $color['label'];
	}
	return $color_options;
}


/**
 * A helper for responsive images
 * @param  int $attachment_id
 * @param  array  $images     an array of classes and their respective image sizes, and returns the image html for all
 * @return string     html containing image elements concatenated
 */
function create_responsive_image( $attachment_id, $images = array(
	'small' => 'medium-large',
	'medium' => 'large',
	'full' => 'full'
) ){
	$output = '';
	$output .= '<div class="responsive-image">';
	foreach( $images as $class => $size ){
		$url = getMediaFromId( $attachment_id, 'url', $size );
		if ( $url ){
			$output .= '<img src="'.$url.'" class="'.$class.'-image" />';
		}
	}
	$output .= '</div>';
	return $output;
}

/**
 * A helper for responsive images
 * @param  int $attachment_id
 * @param  array $proportion width, height, (false for none)
 * @param  array  $images     an array of classes and their respective image sizes, and returns the image html for all
 * @return string     html containing image elements concatenated
 */
function create_responsive_background_image( $attachment_id, $proportion = array( 1, 1 ), $images = array(
	'small' => 'medium-large',
	'medium' => 'large',
	'full' => 'full'
) ){
	$output = '';
	$output .= '<div class="responsive-image responsive-bg-image">';
	if ( $proportion ){
		$output .= '<img class="placeholder" src="http://placehold.it/'.$proportion[0].'x'.$proportion[1] .'" />';
	}
	foreach( $images as $class => $size ){
		$url = getMediaFromId( $attachment_id, 'url', $size );
		if ( $url ){
			$output .= '<div style="background-image: url('.$url.');" class="'.$class.'-image bg-image" ></div>';
		}
	}
	$output .= '</div>';
	return $output;
}
/**
 * Return the page header image
 * @param  id $page_id [description]
 * @return [type]          [description]
 */
function get_page_header_image( $page_id, $default_type = 'parent' ){
	if ( is_object( $page_id ) ){
		$page_id = $page_id->ID;
	}
	$info = get_metabox_options( $page_id, 'page-info');
	$image_type = !empty($info['header_image_type']) ? $info['header_image_type'] : $default_type;
	switch ($image_type ){
		case 'own_image':
			if ( !empty( $info['header_image'] ) ){
				return $info['header_image'];
			} else {
				return '';
			}
			break;
		case 'parent':
			$parent_image = false;
			$page = get_post( $page_id );
			while( $parent_image === false && $page->post_parent !== 0  ){
				$page = get_post( $page->post_parent );
				$parent_image = get_page_header_image( $page );
			}
			return $parent_image;
		case 'none';
		default:
			return '';
	}
}
/**
 * Return the page header image
 * @param  id $page_id [description]
 * @return [type]          [description]
 */
function get_page_sidebar( $page_id, $default_type = 'parent' ){
	if ( is_object( $page_id ) ){
		$page_id = $page_id->ID;
	}
	$info = get_metabox_options( $page_id, 'page-info');
	// default
	$sidebar_type = !empty($info['sidebar']) ? $info['sidebar'] : $default_type;

	switch ($sidebar_type ){
		case 'none':
			return '';
			break;
		case 'parent':
			$parent_sidebar = false;
			$page = get_post( $page_id );
			while( $parent_sidebar === false && $page->post_parent !== 0  ){
				$page = get_post( $page->post_parent );
				$parent_sidebar = get_page_sidebar( $page );
			}
			return $parent_sidebar;
		default:
			return $sidebar_type;
	}
}
/**
 * Return the page sidebar menu
 * @param  id $page_id [description]
 * @return [type]          [description]
 */
function get_page_sidebar_menu( $page_id, $default_type = 'parent' ){
	global $T;
	if ( is_object( $page_id ) ){
		$page_id = $page_id->ID;
	}
	$info = get_metabox_options( $page_id, 'page-info');
	// default
	$menu_type = !empty($info['sidebar_menu']) ? $info['sidebar_menu'] : $default_type;

	switch ($menu_type){
		case 'none':
			return false;
			break;
		case 'parent':
			$parent_sidebar_menu = false;
			$page = get_post( $page_id );
			while( $parent_sidebar_menu === false && $page->post_parent !== 0  ){
				$page = get_post( $page->post_parent );
				$parent_sidebar_menu = get_page_sidebar_menu( $page );
			}
			return $parent_sidebar_menu;
		case 'children':
			return $T->getComponent('page_submenu', array( 'page' => $page_id, 'parent_as_first' => false, 'all_children' => true ) );
		case 'children_parent':
			return $T->getComponent('page_submenu', array( 'page' => $page_id, 'parent_as_first' => true, 'all_children' => true ) );
		case 'siblings':
			$page = get_post( $page_id );
			return $T->getComponent('page_submenu', array( 'page' => $page->post_parent, 'parent_as_first' => false, 'all_children' => true ) );
		case 'siblings_parent':
			$page = get_post( $page_id );
			return $T->getComponent('page_submenu', array( 'page' => $page->post_parent, 'parent_as_first' => true, 'all_children' => true) );
		default:
			return wp_nav_menu(array(
				'menu' => $menu_type,
				'container' => false,
				'menu_id' => ' ',
				'echo' => false
			) );
	}
}
/**
 * Given a site url, it will return a full path to the file
 * @param  string $url
 * @return string $path
 */
function get_path_from_url( $url ){
	if ( strpos( $url, get_bloginfo('url' )) === 0 ){
		$path = substr( $url, strlen( get_bloginfo( 'url' ) ) );
		return ABSPATH . ltrim( $path, '/' );
	} else {
		return false;
	}
}
/**
 * Primarily used for embedding SVGs inline
 * @param  int $attachment_id
 * @return string attachment html
 */
function get_attachment_html( $attachment_id ){
	$svg_path = get_path_from_url( getMediaFromId( $attachment_id, 'url', 'medium' ) );
	if ( ! $svg_path ){
		return false;
	}
	$svg = get_svg_html( $svg_path );
	return $svg;
}
/**
 * Given a path the an SVG file, returns the html as a string
 * @param  string $svg_path
 * @return string svg html
 */
function get_svg_html( $svg_path ){
	// if is url
	if ( strpos( $svg_path, 'http' ) === 0 ){
		$svg_path = get_path_from_url( $svg_path );
	}
	ob_start();
		readfile($svg_path);
	$svg = ob_get_clean();
	$string_to_remove = '<?xml version="1.0" encoding="utf-8"?>';
	if ( strpos( $svg, $string_to_remove ) === 0 ){
		$svg = substr( $svg, strlen( $string_to_remove ) );
	}
	return $svg;
}
/**
 * @param  object $post
 * @return int $parent_id id of the top level parent
 */
function get_top_level_parent_id( $post ){
	while( $post->post_parent !== 0 ){
		$post = get_post( $post->post_parent );
	}
	return $post->ID;
}
