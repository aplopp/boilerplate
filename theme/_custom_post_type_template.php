<?php
// search and replace {POST_TYPE} with your post type
add_action( 'init', function(){
	//example
	$labels = array(
		'name'               => 'Examples',
		'singular_name'      => 'Example',
		'add_new'            => 'Add New',
		'add_new_item'       => 'Add New Example',
		'edit_item'          => 'Edit Example',
		'new_item'           => 'New Example',
		'all_items'          => 'All Examples',
		'view_item'          => 'View Example',
		'search_items'       => 'Search Examples',
		'not_found'          => 'No Examples found',
		'not_found_in_trash' => 'No Examples found in Trash',
		'parent_item_colon'  => '',
		'menu_name'          => 'Examples'
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'capability_type'    => 'post',
		'has_archive'        => false,
		'hierarchical'       => false,
		'menu_position'      => null,
		'rewrite'			 => array( 'slug' => 'news' ),
		'supports'           => array( 'title' )
	);

	register_post_type( '{POST_TYPE}', $args );
	add_action( 'admin_head', function(){ ?>
	<style>
    #adminmenu #menu-posts-{POST_TYPE} .wp-menu-image:before {
       content: "\f232"; /* get from @link {http://melchoyce.github.io/dashicons/} */
    }
    </style>
	<?php } );
});