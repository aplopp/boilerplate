<?php
global $Forms;
if ( ! $Forms ) return;

/* ==== Template: Redirect Page ============================================= */
$Forms->remove_supports( array( 'template' => 'tpl-redirect-page'), array( 'editor', 'revisions' ) );
$Forms->add_template_metabox( 'tpl-redirect-page', array(
	'fields' => array(
		'redirect_to' => array(
			'type' => 'radio',
			'inline' => true,
			'default' => 'id',
			'options' => array(
				'id' => 'Any Post',
				'subpage' => 'First Subpage',
				'file' => 'A Media Library File',
				'url' => 'Any URL'
			)
		),
		'id' => array(
			'title' => 'Post to redirect to',
			'type' => 'post',
			'toggle' => array(
				'redirect_to' => 'id'
			)
		),
		'file' => array(
			'title' => 'File to redirect to',
			'type' => 'media',
			'toggle' => array(
				'redirect_to' => 'file'
			)
		),
		'url' => array(
			'title' => 'Url to redirect to',
			'description' => 'Full url, please. (include http://)',
			'toggle' => array(
				'redirect_to' => 'url'
			)
		),
	)
));
