<?php
if ( ! $Forms ) return;

/* ==== METABOX ============================================= */
$Forms->add_metabox( array( 'post_type' => 'page', 'exclude_template' => array( 'tpl-redirect-page') ), 'page-info', array(
	// use this to specify a particular sidebar for this page
	'fields' => array(
		'---header-image' => array(),
		'header_image_type' => array(
			'type' => 'select',
			'options' => array(
				'none' => 'None',
				'parent' => 'Same as parent',
				'own_image' => 'Has own image',
			),
			'default' => 'none'
		),
		'header_image' => array(
			'type' => 'media',
			'title' => 'Image',
			'get' => 'id',
			'description' => 'Specify a header image to use on this page',
			'toggle' => array( 'header_image_type' => 'own_image' )
		),
		'---title' => array(),
		'hide_title' => array(
			'type' => 'checkbox',
			'checkbox_label' => 'Do NOT show page title at the top of this page?'
		),
		'title' => array(
			'placeholder' => '',
			'description' => 'Defaults to page title',
			'toggle' => '!hide_title'
		),
		'---sidebar' => array(),
		'sidebar' => array(
			'type' => 'select',
			'options' => array(
				'none' => 'None',
				'parent' => 'Same as parent',
				// other sidebars added later
			),
			'description' => 'Do not use sidebars on pages that use full-width Visual Composer rows!'			
		),
		'sidebar_menu' => array(
			'type' => 'select',
			'options' => array(
				'parent' => 'Same as parent',
				'none' => 'None',
				'children' => 'Page hierarchy: this page\'s children',
				'children_parent' => 'Page hierarchy: this page\'s children with this page on top',
				'siblings' => 'Page hierarchy: this page and its siblings',
				'siblings_parent' => 'Page hierarchy: this page and its siblings with their parent page on top',
				// other menus added later
			),
			'default' => 'parent',
			'description' => 'Shows at the top of the page sidebar, or wherever the <strong>[page_sidebar_menu]</strong> shortcode appears.'
		),
		'---misc' => array(),
		'remove_top_padding' => array(
			'type' => 'checkbox',
			'checkbox_label' => 'Each page has some padding on the top by default. This removes it, if necessary.'
		),
		'remove_bottom_padding' => array(
			'type' => 'checkbox',
			'checkbox_label' => 'Each page has some padding on the bottom by default. This removes it, if necessary.'
		)
	)
));
add_action( 'init', function(){
	global $Forms;
	global $wp_registered_sidebars;
	$sidebarField = $Forms->get_metabox( 'page-info' )->get_field( 'sidebar' );
	$sidebarFieldOptions = $sidebarField->get('options');
	foreach( $wp_registered_sidebars as $id => $spec ){
		$sidebarFieldOptions[ $id ] = $spec['name'];
	}
	$sidebarField->set('options', $sidebarFieldOptions );
});

/* ==== CUSTOM COLUMNS ============================================= */
add_filter( 'manage_page_posts_columns', function( $columns ){
	$columns_to_return = array();
	foreach( $columns as $column_id => $column_name ){
		if ( $column_id === 'author' ){
			$columns_to_return[ 'page_layout' ] = 'Page Layout';
		}
		$columns_to_return[ $column_id ] = $column_name;
	}
	return $columns_to_return;
});

add_action( 'manage_page_posts_custom_column' , 'custom_columns', 10, 2 );
function custom_columns( $column, $post_id ) {
	switch ( $column ) {
		case 'page_layout':
			$info = get_metabox_options( $post_id, 'page-info' );

			echo '<div class="page-layout-summary" data-meta=\''.json_encode( $info ).'\'>';
			echo theme_get_page_layout_summary( $post_id, $info );
			echo "<a class='edit-link' href='#edit'>Edit layout options</a>";
			echo '</div>';
			break;
	}
}
function theme_get_page_layout_summary( $post_id, $info){
	global $Forms;
	$mb = $Forms->get_metabox( 'page-info' );

	$output = '';
	// header image
	$header_options = $mb->get_field( 'header_image_type' )->get('options');
	$selected_header_val = !empty( $info['header_image_type'] ) ? $info['header_image_type'] : 'parent';
	$selected_header_name = !empty( $header_options[ $selected_header_val ] ) ? $header_options[ $selected_header_val ] : '<em>not set</em>';
	$output .= '<div class="page-header_image">';
	$output .= 'Header Image: ';
	// could be self, could be parent
	if ( $selected_header_val !== 'none' ){
		$header_image_id = get_page_header_image( $post_id );
		$header_image_src = $header_image_id ? wp_get_attachment_image_url( $header_image_id, 'medium' ) : false;
		$preview_image = $header_image_src ? '<img src="'.$header_image_src.'" class="header-image-preview" alt="Preview" /><img src="'.$header_image_src .'" class="header-image-hover" alt="Hover preview" />' : '<em>(no image)</em>';
		$output .= "<strong>".$selected_header_name.'</strong>';
		$output .= ' '.$preview_image;
	} else {
		$header_image_src = false;
		$preview_image = false;
		$output .= '<strong><em>'.$selected_header_name.'</em></strong>';
	}
	$output .= '</div>';
	// title
	$page_title_value = false;
	if ( !empty( $info['hide_title'] ) ){
		$page_title_value = '<em>hidden</em>';
	} else {
		if ( !empty( $info['title'] ) ){
			$page_title_value = '<strong>"'.$info['title'].'"</strong>';
		}
	}
	$output .= '<div class="page-title">';
	$output .= 'Title: <strong>';
	if ( $page_title_value ){
		$output .= $page_title_value;
	} else {
		$page = get_post( $post_id );
		$page_title_value = 
		$output .= '<em>Same as page title</em>';
		$output .= ' ("'.$page->post_title.'")';
	}
	$output .= '</strong>';
	$output .= '</div>';
	// sidebar
	$sidebar_options = $mb->get_field( 'sidebar' )->get('options');
	$selected_sidebar_val = !empty( $info['sidebar'] ) ? $info['sidebar'] : 'parent';
	$selected_sidebar_name = !empty( $sidebar_options[ $selected_sidebar_val ] ) ? $sidebar_options[ $selected_sidebar_val ] : '<em>not set</em>';
	$output .= '<div class="page-sidebar">Sidebar: ';
	if ( $selected_sidebar_val === 'parent' ){
		$output .= '<strong><em>'.$selected_sidebar_name.'</em></strong>';
		// gets parent sidebar name
		if ( $sidebar_id = get_page_sidebar( $post_id ) ){
			global $wp_registered_sidebars;
			$selected_sidebar_name = !empty( $wp_registered_sidebars[ $sidebar_id ] ) ? $wp_registered_sidebars[ $sidebar_id ]['name'] : 'sidebar does not exist';
		} else {
			$selected_sidebar_name =  $sidebar_options[ 'none' ] ;
		}
		$output .= '  (<em>'.$selected_sidebar_name.'</em>)';
	} else if ( $selected_sidebar_val === 'none' ) {
		$output .= '<strong><em>'.$selected_sidebar_name.'</em></strong>';
	} else {
		$output .= '<strong>'.$selected_sidebar_name.'</strong>';
	}
	$output .= '</div>';

	return $output;
}
if ( !empty( $_GET['post_type'] ) && $_GET['post_type'] === 'page' ){
	// causes all dependencies to be enqueued
	$Forms->get_metabox( 'page-info' )
		->remove_field( '---misc' )
		->remove_field( 'remove_bottom_padding' )
		->remove_field( 'remove_top_padding' )
		->add_field( 'page_id', array(
			'type' => 'hidden'
		))
		->set('is_visible', true );

	add_action('admin_footer', function(){
		global $Forms;
		$mb = $Forms->get_metabox( 'page-info' );
		echo '<div id="page-layout-edit-container" style="display: none;">';
		echo $mb->render();
		echo '</div>';
	});
	add_action( 'admin_enqueue_scripts', function(){
		global $Forms, $T;
		$mb = $Forms->get_metabox( 'page-info' );
		wp_enqueue_style("wp-jquery-ui-dialog");
		wp_enqueue_style( 'edit_page_layout', $T->getDir( 'css/admin/edit_page_layout.css' ) );
		wp_enqueue_script( 'edit_page_layout', $T->getDir( 'js/admin/edit_page_layout.js' ), array( 'jquery-ui-dialog', 'jquery-effects-highlight' ) );
		wp_localize_script( 'edit_page_layout', 'wpVars', array(
			'ajax_url' => admin_url('admin-ajax.php')
		));
	});
}

/* ==== AJAX_ACTIONS for saving PAGE LAYOUT changes ============================================= */
add_action( 'wp_ajax_update_meta_page_info', 'ajax_update_meta_page_info' );
add_action( 'wp_ajax_nopriv_update_meta_page_info', 'ajax_update_meta_page_info' );

function ajax_update_meta_page_info(){
	$info = $_POST['info'];
	foreach( $info as $key => $value ){
		if ( $value === 'false' ){
			$info[$key] = false;
		}
	}
	$page_id = !empty( $info['page_id'] ) ? $info['page_id'] : false;
	if ( ! $page_id ){
		$r['error'] = "No page id sent with request";
	} else {
		unset( $info['page_id'] );
		$r['page_id'] = $page_id;
		$r['success'] = update_post_meta( $page_id, 'page-info', $info );
		$r['summary'] = theme_get_page_layout_summary( $page_id, $info );
		$r['info'] = $info;
		// pass along information to update children at the same time
		$all_descendants = get_pages( array(
			'child_of' => $page_id,
			'posts_per_page' => -1
		));
		$r['children_updates'] = array();
		foreach( $all_descendants as $child ){
			$child_info = get_metabox_options( $child->ID, 'page-info' );
			$r['children_updates'][ $child->ID ] = theme_get_page_layout_summary( $page_id, $child_info );
		}
	}
	echo json_encode( $r );
	die;
}
