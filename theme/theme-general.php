<?php
/* ==== MENUS ============================================= */
function theme_register_menus(){
	// set theme menus here
	$menus = array(
		'main' => 'Main Menu'
	);
	if ( $menus ){
		register_nav_menus( $menus );
	}
}
theme_register_menus();

/* ==== SIDEBARS ============================================= */
function theme_get_sidebars(){
	// set theme sidebars here
	$sidebars = array(
		'page' => 'Page Sidebar'
	);

	// add in dynamic, custom sidebars
	if ( function_exists( 'get_theme_options' ) && $custom_sidebars = get_theme_options( 'theme_options', 'sidebar', 'sidebars' ) ){
		$sidebars = array_merge( $sidebars, $custom_sidebars );
	}
	$sidebar_specs = array();
	foreach( $sidebars as $slug => $sidebar_name ){
		$slug = is_numeric( $slug ) ? sanitize_title( $sidebar_name ) : $slug;
		$sidebar_specs[] = array(
			'id' => $slug,
			'name'	=> $sidebar_name,
			'before_title' => '<h3 class="title">' ,
			'after_title' => '</h3>',
			'before_widget' => '<li id="%1$s" class="widget text-content %2$s" >' ,
			'after_widget'	=> '</li>'
		);
	}
	return $sidebar_specs;
}
function theme_register_sidebars(){
	$sidebars = theme_get_sidebars();
	foreach( $sidebars as $id => $sidebar ){
		register_sidebar( $sidebar );
	}
}

theme_register_sidebars();

/* ==== SCRIPTS AND STYLES ============================================= */
function theme_enqueue_scripts_and_styles(){
	global $T;
	wp_enqueue_style( 'base', $T->getDir( 'css/base.css' ) );
	// split typography out so that it can be used in the WP admin editor, too
	wp_enqueue_style( 'typography', $T->getDir( 'css/typography.css' ), array(), filemtime($T->getABS('css/typography.css')) );
	wp_enqueue_style( 'main', $T->getDir( 'style.css' ), array(), filemtime($T->getABS('style.css')) );
	wp_enqueue_style( 'media-queries', $T->getDir( 'css/media-queries.css' ), array(), filemtime($T->getABS('css/media-queries.css')) );

	wp_enqueue_script( 'debounce', $T->getDir( '_inc/js/jquery-throttle-debounce/jquery.ba-throttle-debounce.min.js' ), array( 'jquery' ), false, true);
	wp_enqueue_script( 'resizedEvent', $T->getDir( '_inc/js/jquery.resizedEvent/jquery.resizedEvent.js' ), array( 'jquery', 'debounce' ), false, true);
	wp_enqueue_script( 'fauxTable', $T->getDir( '_inc/js/jquery.fauxTable/jquery.fauxTable.js' ), array( 'jquery', 'resizedEvent' ), false, true);
	wp_enqueue_script( 'main', $T->getDir( 'js/main.js' ), array( 'jquery', 'resizedEvent'), filemtime($T->getABS('js/main.js')), true);
}
add_action( 'wp_enqueue_scripts', 'theme_enqueue_scripts_and_styles' );

/* ==== ADMIN SCRIPTS AND STYLES ============================================= */
function theme_enqueue_admin_scripts_and_styles(){
	global $T;
	add_editor_style('css/typography.css');
	// wp_enqueue_script( 'customAdmin', $T->getDir( 'js/admin/admin.js' ), array( 'underscore', 'jquery' ));
	// wp_enqueue_style( 'customAdmin', $T->getDir( 'css/admin/admin.css' ) );
}
add_action( 'admin_enqueue_scripts', 'theme_enqueue_admin_scripts_and_styles' );

/* ==== COLOR HELP TAB ============================================= */
function theme_add_colored_help_tabs(){
	global $Forms;
	if ( ! $Forms ) return;

	$colors = theme_get_colored_options();
	$options = array();
	foreach( $colors as $id => $html ){
		$options[] = '<li>'.$html. ' &mdash; use this class: <em>color-'.$id.'</em>';
	}
	$Forms->add_help_tab( array(
		'post_type' => array( 'page', 'post' )
	), 'colors', array(
		'title' => 'Colors',
		'content' => '<h3>Colors</h3><ul>' . implode( '', $options )
	));
}
add_action( 'init', 'theme_add_colored_help_tabs' );

/* ==== IMAGE SIZES ============================================= */
function theme_add_image_sizes(){
	// add_image_size('medium-large', 480, 800, false );
	// add_filter( 'image_size_names_choose', 'theme_custom_image_sizes' );
}
theme_add_image_sizes();
function theme_custom_image_sizes( $sizes ) {
	return array_merge( $sizes, array(
		'medium-large' => __( 'Medium Large' ),
	) );
}

/* ==== THEME SUPPORTS ============================================= */
function theme_add_post_type_supports(){
	add_theme_support( 'post-thumbnails' );
	add_action('init', function(){
		remove_post_type_support( 'page', 'thumbnail' );
	});
}
theme_add_post_type_supports();

/* ==== TINYMCE CUSTOM FORMATS ============================================= */
function my_mce_before_init_insert_formats( $init_array ) {
	// Define the style_formats array
	$style_formats = array(
		// Each array child is a format with it's own settings
		array(
			'title' => 'Large Body Copy',
			'block' => 'p',
			// 'selector' => 'p.large-copy',
			'classes' => 'large-copy'
		),
		array(
			'title' => 'Reversed',
			'selector' => 'h1,h2,h3,h4,h5,h6,p,blockquote,ul,ol',
			'classes' => 'reversed',
		),
	);
	foreach( theme_get_colors() as $color_id => $color ){
		$style_formats[] = array(
			'title' => $color['label'],
			'selector' => 'h1,h2,h3,h4,h5,h6,p,blockquote,ul,ol',
			'classes' => 'color-'.$color_id,
		);
	}
	// Insert the array, JSON ENCODED, into 'style_formats'
	$init_array['style_formats'] = json_encode( $style_formats );

	return $init_array;
  
}
add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' );


function theme_add_editor_buttons($buttons) {

	// array_shift( $buttons ); // remove style dropdown
    array_unshift($buttons, 'styleselect');
    return $buttons;
}
 
add_filter('mce_buttons_2', 'theme_add_editor_buttons');


// allow SVG uploads
function theme_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'theme_mime_types');

// all iframes, wrap in "video-container" so they can be responsified
function wrap_embeds_with_div($html, $url, $attr) {
	if ( strpos( $html, '<iframe' ) === 0 ){
		return '<div class="video-container">' . $html . '</div>';
	}
	return $html;
}
add_filter('embed_oembed_html', 'wrap_embeds_with_div', 10, 3);

