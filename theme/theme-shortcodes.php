<?php
global $Forms;
if ( ! $Forms ) return;

$Forms->add_shortcodes( array(
	'button' => array(
		'icon' => 'hand-o-up',
		'inline' => true,
		'fields' => array(
			'link' => array(
				'title' => false,
				'type' => 'link'
			),
			'color' => array(
				'type' => 'radio',
				'default' => 'green',
				'options' => theme_get_colored_options()
			),
			'is_full_width' => array(
				'title' => false,
				'type' => 'checkbox',
				'checkbox_label' => 'Is full-width'
			)
		)
	),
	'page_sidebar_menu' => array(
		'description' => 'Will pull in this page\'s sidebar menu (set in this page\'s Page Info metabox)',
		'fields' => array(
		)
	)
));