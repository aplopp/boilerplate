<?php
if ( empty( $color )){
	$color = false;
}

$classes = array( 'button' );
if ( $color ){
	$classes[] = 'color-'.$color;
}
if( !empty( $is_full_width ) ){
	$classes[] = 'full-width';
}
if( !empty( $is_reversed ) ){
	$classes[] = 'reversed';
}

$target = !empty( $link['target'] ) ? 'target="_blank"' : '';
$button_text = !empty( $link['title'] ) ? $link['title'] : '';
echo sprintfLink( $link, $button_text, implode( ' ', $classes ) );
