<?php get_header(); ?>
	<?php if( have_posts() ) : while( have_posts() ) : the_post();
		$info = get_metabox_options( 'page-info' );
		$T->theComponent( 'header', 'page' );
		$sidebar = get_page_sidebar( $post, 'none' );
		$classes = array();
		if ( !empty( $info['remove_top_padding'] ) ) $classes[] = 'no-top-padding';
		if ( !empty( $info['remove_bottom_padding'] ) ) $classes[] = 'no-bottom-padding';

		$T->theComponent( 'content', 'page', array(
			'sidebar' => $sidebar,
			'classes' => $classes,
		) );
		
	endwhile; endif; ?>
<?php get_footer(); ?>