jQuery( function($){
	var $window = $(window);
	var $body = $('body');
	// cool little helper to detect touch support (once user touches screen)
	$window.on('touchstart.detect-touch', function() {
		$body.addClass('is-touch');
		$window.unbind('touchstart.detect-touch');
	});
});