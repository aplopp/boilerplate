jQuery( function($){
	var ajax = ( function(){
		var ajax_url = wpVars.ajax_url; // gotta pass this in somehow
		function ajaxCall( data, cb ){
			cb = cb || function(){};
			return $.ajax({
				url: ajax_url,
				method: 'post',
				data: data,
				dataType: 'json',
				success: cb
			});
		}
		var update_page_meta = function( info, cb ){
			return ajaxCall({
				action: 'update_meta_page_info',
				info: info,
			}, cb);
		};
		return {
			update_page_meta: update_page_meta
		};
	}() );

	var $dialog = $( "#page-layout-edit-container" ).dialog({
		modal: true,
		closeOnEscape: true,
		width: 600,
		dialogClass: 'edit_page_layout',
		open: function(event, ui) {
			$('.ui-widget-overlay').bind('click', function(){
				var formValue = pageLayoutForm.get('value');
				rows[ formValue.page_id ].$row.removeClass('editing-layout' );
				$dialog.dialog('close');
			});
		},
		buttons: [
			{
				text: 'Cancel',
				click: function(){
					var formValue = pageLayoutForm.get('value');
					rows[ formValue.page_id ].$row.removeClass('editing-layout' );
					$dialog.dialog( 'close' );
				}
			},
			{
				text: 'Save changes',
				class: 'button-primary',
				click: function(){
					var formValue = pageLayoutForm.get('value');
					$dialog.parent('.ui-dialog').addClass('saving');
					ajax.update_page_meta( pageLayoutForm.get('value'), function(r){
						$dialog.parent('.ui-dialog').removeClass('saving');
						if ( r.error ){
							console.error( r.error );
							return;
						}
						var row = rows[ r.page_id ];
						// give a little time to see the changes.
						setTimeout( function(){
							row.$row.removeClass('editing-layout' );
						}, 2000 );
						row.$editLink.detach();
						row.$summary.html( r.summary );
						row.$editLink.appendTo( row.$summary );
						row.meta = formValue;
						row.meta.page_id = r.page_id;
						$dialog.dialog( 'close' );
						row.$row.effect( 'highlight' );
						console.log( r );
						if ( r.children_updates ){
							_.each( r.children_updates, function( summary, id ){
								var child_row = rows.hasOwnProperty( id ) ? rows[ id ] : false;
								if ( ! child_row ) return;
								child_row.$editLink.detach();
								if ( child_row.$summary.html() !== summary ){
									child_row.$summary.html( summary );
								}
								child_row.$editLink.appendTo( child_row.$summary );
							});
						}
					});
				}
			}
		]
	}).dialog( "close" );

	var pageLayoutForm = Forms.sections.get( 'page-info' );
	var rows = {};
	$('#the-list').find( 'tr' ).each( function(){
		var $row = $(this);
		var $summary = $row.find( '.page-layout-summary');
		var $editLink = $summary.find( '.edit-link');
		var page_id = $row.find( 'input[name="post[]"]' ).val();
		var page_title = $row.find( 'td.title a.row-title' ).text().replace( /^—[ —]+/g, '' );

		// if no meta has been saved, there will be no summary data
		if ( !$summary.data('meta') || typeof( $summary.data('meta') ) === 'undefined' ){
			var meta = _.extend( {}, pageLayoutForm.get('value') );
			_.each( meta, function(value, key ){
				meta[key] = '';
			});
		} else {
			var meta = _.extend( {}, $summary.data('meta') );
		}
		meta.page_id = $row.find( 'input[name="post[]"]' ).val();
		rows[page_id] = {
			$row: $row,
			$summary: $summary,
			$editLink: $editLink,
			title: page_title,
			meta: meta,
			page_id: page_id
		};
		$summary.find( '.edit-link').on( 'click', function(e){
			e.preventDefault();
			var page_meta = rows[page_id].meta;
			if ( ! page_meta.header_image_type ){
				page_meta.header_image_type = 'parent';
			}
			_.each( page_meta, function(value, key ){
				if ( key.indexOf( '---' ) === 0 ) return;
				if ( value === 'false' ) value = false;
				pageLayoutForm.getField( key ).set( 'value', value );
			});

			if ( $dialog.dialog( "isOpen" ) ){
				$dialog.dialog( "close" );
			} else {
				$row.addClass('editing-layout');
				$dialog
					.dialog( "option", "title", 'Edit: '+page_title )
					.dialog( "open" );
			}
		});
	});
	console.log( rows );
});