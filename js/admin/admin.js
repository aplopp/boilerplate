jQuery( function($){
	// VC not saving with WP Editor plugin if html view on page load
	// this fixes that
	$('#poststuff').on('click', '.wpb_switch-to-composer', function(e){
		var activeTab = $('#wp-content-wrap').hasClass('tmce-active') ? 'tmce' : 'html';
		if (activeTab === 'html'){
			if ( wp_editor ){
				wp_editor.setValue( jQuery('textarea#content').val() );
			}
		}
	});
});