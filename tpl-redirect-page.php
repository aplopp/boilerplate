<?php
/*
Template name: Redirect Page
 */
$info = get_metabox_options( 'tpl-redirect-page' );
$redirect = get_bloginfo( 'url' );
if ( !empty( $info['redirect_to'] ) ){
	if ( $info['redirect_to'] === 'file' ){
		if ( !empty( $info['file'] ) ){
			$redirect = wp_get_attachment_url( $info['file'] );
		} else {
			include '404.php';
			die;
		}
	} else if ( $info['redirect_to'] === 'subpage' ){
		$subpages = get_pages( array(
			'child_of' => $post->ID,
			'parent' => $post->ID,
			'sort_column' => 'menu_order',
			'sort_order' => 'ASC'
		));
		if ( $subpages ){
			$redirect = get_permalink( $subpages[0]->ID );
		}
	} else if ( $info['redirect_to'] === 'url' ){
		$redirect = $info['url'];
	} else {
		if ( $info['id'] ){
			$redirect = get_permalink( $info['id'] );
		}
	}
} wp_redirect( $redirect );