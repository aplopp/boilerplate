<?php $footer = get_theme_options( 'theme_options', 'footer' ); ?>
	</div><!-- #content -->
	<footer class="site">
		<?php if ( !empty( $footer['copyright'] ) ){ ?>
		<div class="copyright">
			<?= str_replace( '[year]', date( 'Y' ), $footer['copyright'] ); ?>
		</div>
		<?php } ?>
	</footer>
	<?php wp_footer(); ?>
</body>
</html>