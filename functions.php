<?php
/**
 * Main functions file for the theme.
 */

add_action( 'after_setup_theme', 'custom_theme_setup' );

/**
* Function used mostly to register theme support.
*/
function custom_theme_setup() {

	/* ==== THEME HELPER ============================================= */
	// $T is convenient, $Theme is for backward-compatibility
	// both references to the Theme instance
	global $T, $Theme;
	
	include __DIR__.'/Theme.php';
	// a helper for paths
	$Theme = $T = Custom_Theme::init();
	require_once( 'theme/theme-functions.php' );

	// must be called before init
	require_once( 'theme/theme-plugins.php' );
	
	require_once( 'theme/theme-options-pages.php' );
	require_once( 'theme/theme-shortcodes.php' );
	require_once( 'theme/theme-general.php' );
	require_once( 'theme/page.php' );
	require_once( 'theme/tpl-redirect.php' );
}
/**
 * Helper function, simply returns Theme instance
 */
function T(){
	global $Theme;
	return $Theme;
}
