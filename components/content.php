<?php
// meant to be used inside the loop
if ( ! isset( $post ) ){
	global $post;
} else {
	setup_postdata( $post );
}

if ( !isset( $content ) ){ // if not passed in
	ob_start();
		the_content();
	$content = ob_get_clean();
}
if ( ! isset( $sidebar ) ){ // if not passed in
	$sidebar = false;
}
$has_sidebar = !!$sidebar;

if ( !isset( $classes ) || ! is_array( $classes ) ){
	$classes = array();
}
$classes[] = 'content-container';
$classes = array_unique( $classes );

$tag = !empty( $div_not_article ) ? 'div' : 'article';

if ( $has_sidebar ) $classes[] = 'has-sidebar'; ?>
<div class="<?= implode( ' ', $classes ); ?>">
	<div class="container">
		<<?= $tag; ?> <?php post_class( 'content' ); ?>>
				<?= $content; ?>
		</<?= $tag; ?>>
	<?php if( $has_sidebar ){ ?>
		<aside class="sidebar sidebar-id-<?= $sidebar; ?>">
			<ul class="widgets">
				<?php dynamic_sidebar( $sidebar ); ?>
			</ul>
		</aside>
	<?php } ?>
	</div>
</div>