<?php
global $T;
if ( empty( $title ) ) $title = false;
if ( empty( $subtitle ) ) $subtitle = false;
if ( empty( $image ) ) $image = false;
if ( ! $title && ! $title && ! $image ) return;
$classes = array( 'content-header' );
if ( ! $image ) $classes[] = 'no-image';
?>
<div class="<?= implode( ' ', $classes ); ?>">
	<?php if ( $image ){ ?>
	<div class="image">
		<?= create_responsive_image( $image ); ?>
	</div>
	<?php } ?>
	<div class="text-container">
		<div class="container">
			<?php if ( $title ){ ?>
			<h1 class="content-title"><?= $title; ?></h1>
			<?php }
			$T->theComponent( 'breadcrumbs' );
			?>
		</div>
	</div>

</div>