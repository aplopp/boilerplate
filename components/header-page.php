<?php
global $T;
if ( !isset( $post ) ){
	global $post;
}
$info = get_metabox_options( $post->ID, 'page-info' );

if ( !isset( $title ) ){
	if ( empty( $info['hide_title']) ){
		$title = !empty( $info['title']) ? $info['title'] : $post->post_title;
	} else {
		$title = false;
	}
}
if ( !isset( $subtitle ) ){
	$subtitle = !empty( $info['subtitle']) ? $info['subtitle'] : false;
}
if ( !isset( $image ) ){
	$image = get_page_header_image($post->ID, 'parent');
}

$T->theComponent( 'header', array(
	'title' => $title,
	'subtitle' => $subtitle,
	'image' => $image
));