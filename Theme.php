<?php
class Custom_Theme {
	/* ==== SINGLETON ============================================= */
	private static $instance ;
	public static function init(){
		if ( ! self::$instance ){ self::$instance = new self(); }
		return self::$instance;
	}

	/* ==== INITIALIZE ============================================= */
	protected $directory;
	protected $absolute_path;
	private function __construct(){
		$this->directory = get_bloginfo( 'template_directory' );
		$this->absolute_path = get_template_directory() ;
	}
	/* ==== DIRECTORY/PATH HELPERS ============================================= */
	public function dir( $path = '' ){
		echo $this->getDir( $path );
	}
	public function getDir( $path = '' ){
		return $this->directory . '/' . $path;
	}
	public function ABS( $path = '' ){
		echo $this->getABS( $path );
	}
	public function getABS( $path = '' ){
		return $this->absolute_path . '/' . $path;
	}
	/**
	 * A helper, basically like get_template_part, but with an optional array of variables that can be passed in
	 * @param  string $file_name     the component name
	 * @param  array/string  $template_vars an array of variables to set inside the component, or a refinement of the component name
	 * @param  array  $template_vars Used if the second variable is a refinement of component name
	 * @return [type]                [description]
	 */
	public function theComponent( $file_name, $template_vars = array(), $arg3 = array() ){
		if ( is_string( $template_vars ) ){
			$file_refinement = $template_vars;
			$template_vars = $arg3;
			$file_abs_path = $this->getABS( 'components/'.$file_name.'-'.$file_refinement . '.php' );
			if ( file_exists( $file_abs_path ) ){
				$file_name = $file_name .'-'.$file_refinement;
				extract( $template_vars );
				include( $file_abs_path );
				return;
			}
		}
		
		if ( strpos( $file_name, '/' ) === 0 ){
			$file_abs_path = $file_name;
		} else {
			$file_abs_path = $this->getABS( 'components/'.$file_name.'.php' );
		}
		if ( file_exists( $file_abs_path ) ){
			extract( $template_vars );
			include( $file_abs_path );
			return;
		}
		return;
	}
	public function getComponent( $file_name, $template_vars = array() ){
		ob_start();
			$this->theComponent( $file_name, $template_vars );
		return ob_get_clean();
	}
	/* ==== MISC FUNCTIONS ============================================= */
	/**
	 * @param string $text  The text to be trimmed
	 * @return string       The trimmed text
	 */
	public function excerpt( $text='', $length = 55 ){
		$args = func_get_args();
		if ( sizeof( $args ) === 0 || is_integer( $args[0]) ){
			global $post;
			$text = $post->post_excerpt ? $post->post_excerpt : $post->post_content;
			if ( is_integer( $args[0] ) ){
				$length = $args[0];
			}
		}
	    $text = strip_shortcodes( $text );
	    $text = apply_filters('the_content', $text);
	    $text = str_replace(']]>', ']]&gt;', $text);
	    $excerpt_length = apply_filters('excerpt_length', $length );
	    $excerpt_more = apply_filters('excerpt_more', ' ' . '[...]');
	    return wp_trim_words( $text, $excerpt_length, $excerpt_more );
	}
	/**
	 * attempts to duplicate the important part of apply_filters( 'the_content' )
	 * @param  string $text [description]
	 * @return formatted string
	 */
	public function content( $text = '' ){
		if ( sizeof( func_get_args() ) === 0 ){
			global $post;
			$text = $post->post_content;
		}
		return prepend_attachment( shortcode_unautop( wpautop( convert_chars( convert_smilies( wptexturize( $text ) ) ) ) ) );
	}
	public function get_template_name(){
		return basename( get_page_template(), '.php' );
	}
}
