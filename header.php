<?php global $T; ?>
<!DOCTYPE html>
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<!-- script for no-js class on html element -->
	<script>(function(H){H.className=H.className.replace(/\bno-js\b/,'js')})(document.documentElement)</script>
	
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title><?php
		// is WP SEO enabled? (will cause duplicated title)
		if ( ! defined( 'WPSEO_FILE' ) ) {
			wp_title( '|', true, 'right' );

		
			// Add the blog name.
			bloginfo( 'name' );
		
			// Add the blog description for the home/front page.
			$site_description = get_bloginfo( 'description', 'display' );
			if ( $site_description && ( is_home() || is_front_page() ) )
				echo " | $site_description";
		} else {
			wp_title();
		}
		?></title>
	<?php if ( $favicon = get_theme_options( 'theme_options', 'favicon' ) ){
		$image_name = basename( $favicon );
		$extension = substr( $image_name, strpos( $image_name, '.' ) + 1 ); ?>
	<link rel="icon" type="image/<?= $extension; ?>" href="<?= $favicon; ?>">
	<?php } ?>
	
	<?php wp_head(); ?>
	<!--[if lt IE 9]>
		<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<script src="<?php $T->dir('_inc/respond.min.js'); ?>"></script>
	<![endif]-->
</head>
<?php $header = get_theme_options( 'theme_options', 'header' ); ?>
<body <?php body_class(); ?>>
	<header class="site">
		<div class="container">
			<div class="logo">
				<a href="<?php bloginfo( 'url' ); ?>" title="<?php bloginfo( 'name' ); ?>">
					<?= $header['logo']; ?>
				</a>
			</div>
			<nav class="main-menu">
			<?php wp_nav_menu( array(
				'theme_location' => 'main',
				'container' => false
			)); ?>
			</nav>
		</div>
	</header>
	<div id="content">
	
